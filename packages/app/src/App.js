import React, { useEffect, useState } from 'react'
import Header from './Header'

const App = (props) => {
  const [state, setState] = useState({
    message: null,
    loading: true,
    error: false,
  })

  useEffect(() => {
    const request = async () => {
      setState({ ...state, loading: true })

      // const response = await fetch(process.env.REACT_APP_GRAPH_ENDPOINT, {
      //   method: 'post',
      //   headers: {
      //     'content-type': 'application/json',
      //   },
      //   body: JSON.stringify({
      //     variables: {},
      //     query: `query {
      //       message
      //     }`,
      //   }),
      // })

      // // 2. alternatively retrieve data directly from the api
      const response = await fetch(process.env.REACT_APP_REST_ENDPOINT,{
        headers: {
          'content-type': 'application/json',
        }
      })

      const { data } = await response.json()

      setState({ ...state, ...data, loading: false })
    }

    request()
  }, [])

  if (state.loading) {
    return null
  }

  return <Header message={state.message} />
}

export default App
